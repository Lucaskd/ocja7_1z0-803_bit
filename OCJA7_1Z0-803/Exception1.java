
public class Exception1{
  
    public static void main(String ...args ){
     
        try{
          throw new Ex2();
        }catch(Ex1 e1 ){
          System.out.println(" Ex2 ");
        }catch(Exception e3 ){
            System.out.println(" Ex3 ");
        }finally{
          System.out.println("finally ");
        }
          
    }
}
class T {

     void soma()throws Ex2{
         throw new Ex2();
     }

}

class Ex1 extends Exception {

}

class Ex2 extends Ex1{

}

class Ex3 extends Ex2{

}

