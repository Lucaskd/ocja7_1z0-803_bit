/** Eu posso redefinir meto como abstract na clase filha;*/
public class ClassAbstrata {
    Animal a = new novo();
    a.barulho();
}

class Animal{
public void barulho(){}

}
class Gato extends Animal{
public void barulho(){}
}

abstract class  novoAnimal extends Animal{
    public abstract void barulho();
}

class novo extends novoAnimal {
  public void barulho(){ System.out.println("Olá"); }
  
}