import static java.lang.System.*;

public class Usandolacos{
   

   
   public static void main(String ...args){
       for (int i = 0 ; i < 5 ; ++i){
         out.println(i);
       }
    }
}
class EnhancedFor{

 public static void main(String ...args){
   
     java.util.ArrayList<Integer> ni = new java.util.ArrayList<>();
     ni.add(1);
     ni.add(2);
     ni.add(3);
     ni.add(4);
     
    for( Integer i : ni ) {
       System.out.println("" + ni);
    }
    //Interator, Interator, I
    //for(Iterator<String> i = ni.Iterator(); i.hasNext(););
    
  }

}
class UsandoFor{
   
   public void metodo(){}
   public static void main(String ...args){
      
      //for(){} não compila
      //for(;){} não compila
     //  for(int a, b,c, d;){}/ não ompila tem que inicializar
      
     /** Declarações de treŝ vaariáveis do tipos int e inicializada as tres; Repare que o "," separa as declarações e inicializada */
     // for(;;){}//ok
        //for(int a = 1, b = 2,c= 3, d= 4;;){}//ok;

       /** Declaração três variáveis de tipos diferentes einicializando as três variáveis ja declaradas */ 
       int z; 
       double b2;
       boolean b3 ;
       
       //for(z=2,  b2 = 2, b3=true;;){}//ok
       
       /** Podemos inicializar com tipos diferentes */
       // for(float db=0, int z1=2; z1 < 10 ; z1++){}//ok
       
       /**
        * - no campo de atualização podemos usar os operadores de incremento podemos usar qualquer trecho 
        * - No campo de condição podemos passar qualquer expresão que resulte em boolean */
        
        UsandoFor m = new UsandoFor();
       
        
        for(int x = 0 ; x > 0 ; ){}//ok demproblemas
        for(int x = 0 ; x > 0 ; System.out.println("")  ){} //chama print
        for(int x = 0 ; x > 0 ; m.metodo() ){}//chama metodo
    }
}
