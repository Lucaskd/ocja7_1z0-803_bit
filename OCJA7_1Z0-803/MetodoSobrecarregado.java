class Base{
    String nome;
    
    Base(){
        testa();
      nome = "Inicializado";   
    }
    
    public void testa(){
      System.out.println("do while");
    }
}

class BaseX extends Base{
    static final public String x1 = "";
    BaseX(String ... x){ System.out.println(" BaseX(String ... x) "); }
    BaseX(String y){ System.out.println("BaseX(String y)" ); }
    
    BaseX(){
    this(x1);
    }
    
    public void testa(){
      System.out.println("do while" );
    }
}

public class MetodoSobrecarregado{

    static final public  void main(String ...ags ){
        BaseX bx = new BaseX("");
        
        double d = 1;
        int i = 1;
        byte b = 10;
        System.out.println("Ola - " + get("string") );
       
    }
    
    public static String get(String x){ return x; }
    
    public static String get(Object x){ return x.toString();}
         
    public static int get(int x){
        if(x > 0){
            System.out.println(" int - "+ x );
          return x;
        }    
        throw new RuntimeException("Não pode ser zero !");
    }
    
    public static double get(double x){
        if(x > 0){
            System.out.println(" double - "+ x );
          return x;
        }    
        throw new RuntimeException("Não pode ser zero !");
    }
    
    public static byte get(byte x){
        if(x > 0){
            System.out.println(" byte - " + x );
          return x;
        }    
        throw new RuntimeException("Não pode ser zero !");
    }
}
