import static java.lang.System.*;

public class IgualdadeEntreStrings {
  
  

    
    public static void main (String ... args ) {
       
        /** Teste a igualdade entre Strings e outros objetos usando == e equals()  */
        
        String str1 = new String ("A");// Strings criadas com o operador new não são colocadas no pool de String automaticamelte;
        String str2 = new String ("A");
        
         String str3 = "4";
         String str4 = "4";
        
        System.out.println(str1 == str2); // false  
        System.out.println(str3 == str4);//true
        /** Quando usado o operador == : ele verifica se já exite um referencia da mesma string no pooll. Não ver o conteudo, para saber e mesmo 
         *  conteudo usa o operador eguals */
        
        String nome1 = "LC";
        String nome2 = "LC";
        String nome3 =  new String("LC");
        
        System.out.println(nome1 == nome2); // true, mesma referencia.  
        System.out.println(nome1 == nome3); // false,não é a mesma referencia. estão em pool diferentes; 
        System.out.println(nome1.equals(nome3) ); // true, mesmo conteúdo; ele verifica o conteúdo e não a referencia;  
        System.out.println(nome3.equals( new String("LC") ) );//
        
        System.out.println("----------------------------------------------");//
        
        System.out.println("String".replace("g","G") == "String".replace("g","G") );
        System.out.println("String".replace("g","G").equals("String".replace("g","G") ));
        
        /**Quando concatenamos literais a String resultante também será coclocado no pool */
        
        String literal = "b" + "a";
        out.println("ba"==literal);//literal string
        
        /** Não funciona quando é um literal e uma referencia exemplo :*/
        String referencia = "a";
        String literalMiasReferencia = "b" + referencia; 
        out.println("ba" == literalMiasReferencia);//literal string
        
    }

   
 
}
