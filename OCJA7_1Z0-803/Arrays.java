public class Arrays {
  
    /** Usando e criandos  Arrys: Declarar
        Inicializar,
        Acessar,
        Percorrer,
    */
  
   /**
     Diferença entre o posicionamento dos colchetes: int [] i, j; <- aqui i e j são ambos array de inteiros.
     int i [], j; // aqui apenas i é uma matriz de inteiros. j é apenas um inteiro.    
   */
   public static void main(String ...args ){
       
       //criar arrys
        boolean[] b45 = new boolean[2];
        
        int numeros1[];//size = 0
        int []numeros2;//size = 0
        int numeros [] = new int [2];//obrigado a passar o tamanho;//size = 2
        int [] n = new int [] {1,2,3,4,5,6,3};//ok
        int [] n2 = {1,2,3,4,5,6,3};//ok
        int [] n3;
        n3 = new int [] {1,2,3,4,5,6,3};//ok
         
          int [] n4 ;
          //n4 = {1,2,3,4,5,6,3};//não podeser atribuido dessa maneira;
          int a1 [] [] = new int [] [] { {1,2,3},{4,5,6} };//ok
          int [] b [] =  { {1,2,3},{4,5,6} };//ok
          int b1 [] [] =  { {1,2,3},{4,5,6} };//ok
          
          int a2 [] [] = new int [] [] { {1,2,3},{4,5,6},{7,8,9} };//ok
          a2[0][2] = 150;
          System.out.println(" - > " + a2[0][2]);
          
        /** Em cada posição de um array de tipos não primitivos é guardada uma variável não primitiva exemplo: null.
         *  
         *  - Em tipos não primitivos e inicializado com 0; 
         *  - Em objetos são inicalizados por defult com null;
         *  - Não podemos delcara o tamnho do array do lado esquerdo: Alunoa [10] = new Alunos; não compila;
         *  - Tentar acessar um propriedade de um array de objetos que estja nula da nullPoint;
         *  - O polimorfismo funciona normalmente, portanto funciona igualmente para interfaces. É possível adicionar filhos de super class;
         *  - Objetos é sempre baseado em referências, objeto não será copiado, mas somente sua referência passada: para outro;
         *  - Casting de arrays Não há ::casting:: de arrays de tipo primitivo, portanto não adianta tentar:
         *  - Já no caso de referências, por causa do polimorfismo é possível fazer a atribuição sem casting de um array para outro tipo de array:
         *  - Arrays não podem crescer em tamanho depois de criados. ArrayLists pode fazer isso.
          */
         
          /**Não há, Casting de arrays  */
          int[] valores = new int[10];
          //long[] vals = valores;  não compila
          
          /** casting de objetos funcionão normalmente; */
            String arrysDeString [] =  {"2","3","4"};
            Object [] castDeObjetos = arrysDeString;
           
          
          /** Erros :  Alunoa [10 = new Alunos [] */
           Aluno [] a = new Aluno [10];
          
           /** Ou você passa o tamanho, ou passa os valores */
            int y[]; //= new int[3] {0,3,5}; caso contrario não compila;
           
           int x[] = new int[0];
            // int x[]  = new int[] {0,3,5};
            // int x[]  = int new  {0,3,5};

           int x1[] = new int[3];
           for(int i=x.length;i>=0;i--) x[i]=i*2;//erro em tempo de execução;
            System.out.println("Fim!");//
            
            for(int i=x.length;i>=0; --i /*Não da erro! arrays.size - 1 */) 
             x[i]=i*2;//erro em tempo de execução;
            System.out.println("Fim!");// 
            
            /**  maneiras adiante são declarações e inicializações válidas para um array  */
            
                 int[] array1 = new int[10];
                 int array2[] = new int[10];    
                 int array3[] = {1, 2, 3};
                 int[] array4 = new int[]{1, 2};
                
                 /** não são inicializações válidas **/
                 
                 //int[] array5 = new int[]; falta o tamanho do array. Se não inicializar de preencher ou new int [3] ou new int {}
                
                  //int array6[] = new int[];//mesmo de cima
                
                 // int[] array5 = new int[2]{1, 2}// ou coloca new int[2] ou new int {1,2,3}
                 
                   //int[] array7 = int[10]; falta o new
                
                 //int[] array8 = new int[1, 2, 3]; erro
                
                 //int array9[] = new int[1, 2, 3];
                
                
            int [] [] array10 = {{0}, {0, 1}, {0, 1, 2}, {0, 1, 2, 3}, {0, 1, 2, 3, 4}};
            
    
   }

 class Aluno{
 
 }

}
