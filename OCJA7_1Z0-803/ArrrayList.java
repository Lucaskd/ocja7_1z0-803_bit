import java.util.*;

public class ArrrayList {
   
    public static void main(String ...args ){
        
        ArrayList<String> lista =  new ArrayList<>();
            
            /** 
              ArrayList
                 add(); adicionar o elemento especifico no fim da lista;
                 add(inex , elemento); insere o elemento na posição especifica desta lista;
                 
                 addAll(collection); adiciona um lista dentro de outra lista e matem a ordem;
              	 addAll(int index, Collection<? extends E> c);add um lista dentro da outra em uma posição especifica;
                 
              	 clear(); remove todos os elementos da lista;
                 
              	 contains(Object o); retorna true se o elemento estar na lista;
                 containsAll(); verifica se um elemento está dentro d alista;
                 
                 get(int index); devolve o elemento em uma posição especifica;
                 isEmpty(); true se não tem elementos;
                 
                 indexOf(Object o); devolve o index da primeira ocorrencia;
                 lastIndexOf(Object o); devolve o index da ultima ocorência do especifico elemento;
                 
                 remove(int index); remove o elemento de uma posição especifica;
                 remove(Object o);remove um determinado objeto;
                 removeAll(Collection<?> c); remove um coleção de ocorencias; todas as ocorencias do elementos;
                 set(int index, E element); adiciona um elemento em uma posição especifica;
                 size(); tamanho da lista;
                 subList(int fromIndex, int toIndex); retorna um lista com elemento em uma determinada posição;
                 toArray();para arrry
                 retainAll(Collection<?> c)
                 
             */
            lista.add("A");
            lista.add("C");
            lista.add("B");
            lista.add("D");
            lista.add("E");
            lista.add(1,"B");
        
            new ArrayList();//pode
            //new ArrayList;//pode
        
        ArrayList<String> l =  new ArrayList<>();
            l.add("1");
            l.add("2");
        l.addAll( lista.subList( 3,4) );
        System.out.println(l);
        lista.addAll(l );
        //lista ->  ABCBDE12DE , l ->12DE
        System.out.println(lista);
        
        lista.removeAll(l);
        System.out.println(lista);
        
        System.out.println(lista.retainAll(l));// verifica 
    }
}
