import static java.lang.System.*;

public class ComparandoReferencias {
   
    public static void main(String ...args ){
    
        Pessoa p1 = new Pessoa("Zé",18);
        Pessoa p2 = new Pessoa("Zé",18);
        Pessoa p3 = p2;
        /** == compara a posição na memoria para obejetos: String, Minhas Classes: new String("a") == new String("a") = false; new Integer (1) == new Integer (1) - false;
          equals - quando usado para comparação de objetos o comportamento padrão é fazer a simples comparação como o iguas;
          
        */
        out.println(p1 == p2);// false
        out.println(p1 == p1);
        out.println(p2 == p3);
        out.println( p1.equals(p2) );// nã está na mesma posção de memoria por defalt
        out.println(p2.equals(p3));// tru
        Object o = new Object();
        out.println(p2.equals(o));// false
    }
}

class Pessoa{
 
    public Pessoa(){
    }
    
    public Pessoa(String n, int i ){
     this.nome = n; this.idade = i; 
    }
    String nome;
    int idade;

}