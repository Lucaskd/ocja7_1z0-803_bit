import static java.lang.System.*;

public class UtilizandoSwitch {
      final static int CONSTANTE = 20 ;// deve ser inicializada aqui;
       
       static{
        //CONSTANTE  = 20;// não pode dazer isso se estiver usando essa constante em um case;
       }
        
    public static void main(String ...args ){
        
        /** Em cadas case , só podemos usar como valor uma variavel literal e uma variavel final atríbuida como valor literal. */
         
        int variavel  = 20 ;
         
        /** O argumento switch deve ser uma variável composta com o tipo int ou wrapper compativel com int: int , char, short, byte, String e enum; E o tipos devem ser compativeis */
        int op = 6;
        byte op1 = 1;
        short op2 = 1;
        
        /**  ints e derivados - int , char, short, byte **/
        /** Constantes em casa:  para ser considerada uma constante em um case a variavel, alem de ser final deve ter sido inicializada em sua inicialização*/
        /** Variavel não decalrada que não seja uma constante não pode fica case e o valor null */
        
        switch(op){
            case CONSTANTE : out.println("switch B");// constante em uso;
            case (2/5) : out.println("1"); break;
            default: out.println(" default "); break; // caso não tenha um break e os outrs abixo e executados;
            case 2 : out.println("2 ");break;
            case 3 : out.println("3 ");break;
            
        }
       
        /*
        switch(op){
            default: out.println("switch com interios..");
        }
         */
        /**  String **/
       
        String opcao = "A";
         /*
        switch( opcao ){
            case "A" : 
               int i =10 ;
               if(true){ op ++; } else { op++; ++i; };
            case "B" : out.println("switch B");
            default: out.println("switch com Strings..");
        } */
    }
}

