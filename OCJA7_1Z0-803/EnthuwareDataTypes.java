
import static java.lang.System.*;

 /** StringBuilder  não tem metodo trim */

public class EnthuwareDataTypes{
     private int side = 0 ;
   
    public static void main (String ...args){
        
        InitTest i = new InitTest();
        EnthuwareDataTypes sq = new EnthuwareDataTypes();
        sq.side = 10;//pode porqu
       
        String String = "string isa string";
        out.println(String.substring(3, 6));
        
    }
}


//What will be the output of the following program (excluding the quotes)?


class SubstringTest{
   public static void main(String args[]){
      String String = "string isa string";
      System.out.println(String.substring(3, 6));
   }
}


// switch não aceita valores duplicados
class pegadinha2{

     int value = 1 ;//,000,000; //1 erro 
        //para o java 1_000_000 e igual a 1000000; 13 e o mesmo que 1_3
        /** switch(value){
            case 1_000_000 : System.out.println("A million 1"); //2
                break;
            case 1000000 : System.out.println("A million 2"); //3
                break;
        } **/
        

}
//What will be the result of attempting to compile and run the following program?

class TestClass{
   public static void main(String args[ ] ){
      StringBuilder sb = new StringBuilder("12345678");
      sb.setLength(5);
      sb.setLength(10);//independente do tamanho das string contida o tamanho é 10
      System.out.println(sb.length());
   }
}


//Which of the following statements can be inserted at // 1 to make the code compile without errors?

class InitTest{
   static int si = 10;
   int  i;
   final boolean bool; //final sempre inicalizada com valor explicito
   // 1
   
   //InitTest() { si += 10; }// ok, mais não compila porque bool não foi inicializada;
   //{ si = 5; i = bool ? 1000 : 2000;}/// não usa bool por não ter sido inicializado;
   { bool = (si > 5); i = 1000; }
}