package heranca;

public class Heranca2{
    
  public static void main(String ...args ){
    
      Conta c1 = new Conta();
      Conta c2 = new ContaC();
      Conta c3  = new ContaP();
      
      boolean a1 = c1 instanceof ContaC;
      boolean a2 = c2 instanceof ContaC;
      boolean a3 = c3 instanceof ContaC;
      boolean a4 = c3 instanceof Object;//sempre true;
      
      System.out.println(" A1 = " + a1 + " A2 "+ a2 + "  A3 = " + a3 + " A4 "+ a4 );
      
       imprimir(c1);
      // imprimir(c2 );
       //imprimir(c2);
       //imprimir(new Object() );
  }
  
  public static void imprimir(Conta  c ){
       // se a variavel comparada estiver null da false usando intanceof;
      if(c instanceof Conta ){
         System.out.println("c instanceof Conta - sim");
      }if(c instanceof ContaC) {
         System.out.println("c instanceof ContaC - sim");
      }if(c instanceof ContaP ){
         System.out.println("c instanceof ContaP - sim");
        }
  }
    
}

//abstract
 class  Conta{
    
  public Conta(){
       System.out.println("Defualt..");
  }
  
  public Conta(String op){
     
       System.out.println("Operação relizada por : " + op);
  }
  
  
  public void depositar(){
    System.out.println("Deposita na conta!"); 
  }
}

class ContaP extends Conta{
    
  
  
  public ContaP(){
      super("Operaçao em P");
       System.out.println("Defualt..P");
        
  }
  
  public void depositar(){
    System.out.println("Deposita na contaP!"); 
  }

}

class ContaC extends Conta {
    public ContaC(){
        super("Operaçao em C");//construtor da classe mãe tem que ser chamado primeiro;
       System.out.println("Defualt..C");
       
  }
  public void depositar(){
    System.out.println("Deposita na contaC!"); 
  }
}