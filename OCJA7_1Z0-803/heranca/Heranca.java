package heranca;

public class Heranca{
    
    public static void main(String ...args ){
    
        Pessoa p = new Dev();
        p.ola();
        int n = p.numero;
        System.out.println("numero p -> dev ! "+ n); 
     
        
        Pessoa pApp = new DevApp();
        pApp.ola();
        
        int n2 = pApp.numero;
        System.out.println("Pessoa p -> DevApp ! "+ n2);
        
        Dev d = new Dev();
        d.soma(1,2);
        int n1 = d.numero;
           d.ola("","","");
        System.out.println("numero dev -> dev ! "+ n1); 
        
        DevApp app = new DevApp();
        app.soma(1,2);
        int np = app.numero;
        System.out.println("numero DevApp -> DevApp ! "+ np); 
        
        Dev d2 = new DevApp();
        int d3 = d2.numero;
        System.out.println("d3 ! "+ d3); 
        
        /**/

        Pessoa p01 = new Dev();//ok
        Pessoa p02 = new DevApp();//ok
        
        Dev d01 = new DevApp();//ok
        
        Dev d02 =(Dev) new Pessoa();///compila, mas da erro e tempo de execuçã; java.lang.ClassCastException:
        d02.ola();
        
        DevApp dvApp =(DevApp) new Dev();///compila, mas da erro e tempo de execuçã; java.lang.ClassCastException:
        
    }
}

class Pessoa{
    static int numero = 40;
    
   public void ola(){
     System.out.println("Ola Pessoas! ");
   } 
}

class Dev extends Pessoa{
   static int numero = 200;
   
   public void ola(){
     System.out.println("Ola Dev! ");
   } 
   
    public String ola(String...as){
     System.out.println("Ola Dev! ");
     return as.toString();
   } 
   
   public void soma(int x, int y ){
     System.out.println("Dev soma! " + x + y);
   } 
   
   public void ola(String param ){
     System.out.println("Ola Dev! " + param);
   } 
}

class DevApp extends Dev{
 static int numero = 150;
  public void mult(int x ){
     System.out.println("Ola Dev! " + x * x);
  } 
   
  public void ola(){
     System.out.println("Ola DevApp! ");
  } 
}