
public class Staticos {
     public static void main (String...args){
        // A b  = new B();
         B b1 = new B();
         
        // b.p();
         //b1.salvar(11);
        // b1.salvar();
     }
}

class A {
  public A(){
    this(12);//tem que ser primeiro a ser chamando.
    System.out.println("Construtor A  se parametro  -");
    
  }
  
  public A(int x ){
    System.out.println("Construtor A  com parametro - " + x);
  }
  
  
  static int PI = 48;
  
  static void  p(){
     System.out.println("-------- B"+ PI);
  }
    
  public void salvar(){
       System.out.println("-------- B"+ PI);
  }
}


class B extends A{
  
  public B(){ super(150);/** Construindo objeto; constutor vazio não vai ser chamando*/ System.out.println("Construtor B sem parametro "); }
  
   public B(int x){ ; System.out.println("Construtor B com parametro - " + x); }
    
  static int PI = 4;
  
  static void  p(){
    System.out.println("------- A " + PI);
  }
  
  public void salvar(){
    System.out.println("-------- A "+ PI);
  }
  
  public void salvar(int x){
    System.out.println("-------- A "+ x);
  }
}
