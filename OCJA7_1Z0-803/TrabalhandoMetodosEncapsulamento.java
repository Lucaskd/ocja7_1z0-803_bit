
public class TrabalhandoMetodosEncapsulamento{
  
    public static void main(String ... args ){
       TrabalhandoMetodosEncapsulamento t = new TrabalhandoMetodosEncapsulamento();
       byte b =1;
       int v = 90/4;
        t.dv2(b);
        t.dv1(v);
        System.out.println("");
    }
    
    public int dv6(byte x){
        System.out.println("byte");
        return x;
    }
    
    public int dv2(short x){
        System.out.println("short");
        return x;
    }
    
    public int dv2(char x){  
        System.out.println("char");
        return x;
    }
    
    public int dv2(int x){ 
         System.out.println("int");
        return x;
    }
    
    public long dv1(long x){ 
        System.out.println("int");
        return x;
    }
    
    public float dv1(float x){  
         System.out.println("float");
        return x;
    }
    
     public double dv1(double x){ 
          System.out.println("double");
        return x;
    }
}
