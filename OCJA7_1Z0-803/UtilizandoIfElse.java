import static java.lang.System.*;

public class UtilizandoIfElse{
 
 public static void main(String ...args ){
    boolean logico = true;
    int valor = 10;
    /**
     if(logico) {
       out.println("Executando.. if");
           if(logico) out.println("Executando.. segundo if");
           else
            out.println("Executando.. segundo esle");
       
     }
      else
        out.println("Executando.. esle"); **/
   
        if(logico )
            if(true)
                if(true)
                    if(true) out.println("Executando.. esle");
                else out.println("Executando.. esle");
            else 
        out.println("Executando.. segundo ");
 }
 
/** Codigos java não compila se o compilador perceber que aquele código não sera executao*/
 class TesteNaoCompila{
    public int valor(){
     return 5;
     //out.println("Executando.. segundo "); não compila porque el nunca iria ser chamando;
    }
    
    public int valor(int x){
        if(x > 200 ) {
           return 5;//missing retrun startament: tem que ter retorn fora do if; ou fora do if;
        }
      throw new RuntimeException();
    }
 }
}
