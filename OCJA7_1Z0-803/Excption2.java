public class Excption2 {
   
    public static void main(String ...args ){
     
        new Test().dooing();
    
    }
}

class E1 extends Exception{ }

class E2 extends E1 { }

class Test{
   public void  dooing (){
      try{
         throw new E2();
      }
      catch(E1 e){
         System.out.println("E1");
      }
      catch(Exception e){
         System.out.println("E");
      }
      finally{
         System.out.println("Finally");
      }
   }
}

class TestClass{
    void probe(Integer x) { System.out.println("In Integer"); } //2
    
    void probe(Object x) { System.out.println("In Object"); } //3 
    
    void probe(Long x) { System.out.println("In Long"); } //4
    
    public static void main(String[] args){
        String a = "hello"; 
        new TestClass().probe(a); 
    }
}


